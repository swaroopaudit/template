final Map<String, dynamic> lightTheme = {
  "applyElevationOverlayColor": false,
  "brightness": "light",
  "buttonTheme": {
    "alignedDropdown": false,
    "colorScheme": {
      "background": "#fffbfcfe",
      "brightness": "light",
      "error": "#ffba1a1a",
      "errorContainer": "#ffffdad6",
      "inversePrimary": "#ff74d1ff",
      "inverseSurface": "#ff2e3133",
      "onBackground": "#ff191c1e",
      "onError": "#ffffffff",
      "onErrorContainer": "#ff410002",
      "onInverseSurface": "#fff0f1f3",
      "onPrimary": "#ffffffff",
      "onPrimaryContainer": "#ffffffff",
      "onSecondary": "#ffffffff",
      "onSecondaryContainer": "#ff091e28",
      "onSurface": "#ff191c1e",
      "onSurfaceVariant": "#ff40484d",
      "onTertiary": "#ffffffff",
      "onTertiaryContainer": "#ff1b1736",
      "outline": "#ff71787d",
      "outlineVariant": "#ffc0c7cd",
      "primary": "#ff006688",
      "primaryContainer": "#ff196381",
      "scrim": "#ff000000",
      "secondary": "#ff4e616c",
      "secondaryContainer": "#ffd1e6f3",
      "shadow": "#ff000000",
      "surface": "#fffbfcfe",
      "surfaceTint": "#ff006688",
      "surfaceVariant": "#ffdce3e9",
      "tertiary": "#ff5f5a7d",
      "tertiaryContainer": "#ffe5deff"
    },
    "height": 36,
    "layoutBehavior": "padded",
    "minWidth": 88,
    "padding": {"bottom": 0, "left": 16, "right": 16, "top": 0},
    "shape": {
      "borderRadius": {
        "bottomLeft": {"type": "elliptical", "x": 2, "y": 2},
        "bottomRight": {"type": "elliptical", "x": 2, "y": 2},
        "topLeft": {"type": "elliptical", "x": 2, "y": 2},
        "topRight": {"type": "elliptical", "x": 2, "y": 2},
        "type": "only"
      },
      "side": {
        "color": "#ff000000",
        "strokeAlign": -1,
        "style": "none",
        "width": 0
      },
      "type": "rounded"
    },
    "textTheme": "normal"
  },
  "canvasColor": "#fffbfcfe",
  "cardColor": "#fffbfcfe",
  "colorScheme": {
    "background": "#fffbfcfe",
    "brightness": "light",
    "error": "#ffba1a1a",
    "errorContainer": "#ffffdad6",
    "inversePrimary": "#ff74d1ff",
    "inverseSurface": "#ff2e3133",
    "onBackground": "#ff191c1e",
    "onError": "#ffffffff",
    "onErrorContainer": "#ff410002",
    "onInverseSurface": "#fff0f1f3",
    "onPrimary": "#ffffffff",
    "onPrimaryContainer": "#ffffffff",
    "onSecondary": "#ffffffff",
    "onSecondaryContainer": "#ff091e28",
    "onSurface": "#ff191c1e",
    "onSurfaceVariant": "#ff40484d",
    "onTertiary": "#ffffffff",
    "onTertiaryContainer": "#ff1b1736",
    "outline": "#ff71787d",
    "outlineVariant": "#ffc0c7cd",
    "primary": "#ff006688",
    "primaryContainer": "#ff196381",
    "scrim": "#ff000000",
    "secondary": "#ff4e616c",
    "secondaryContainer": "#ffd1e6f3",
    "shadow": "#ff000000",
    "surface": "#fffbfcfe",
    "surfaceTint": "#ff006688",
    "surfaceVariant": "#ffdce3e9",
    "tertiary": "#ff5f5a7d",
    "tertiaryContainer": "#ffe5deff"
  },
  "dialogBackgroundColor": "#fffbfcfe",
  "disabledColor": "#61000000",
  "dividerColor": "#1f191c1e",
  "focusColor": "#1f000000",
  "highlightColor": "#66bcbcbc",
  "hintColor": "#99000000",
  "hoverColor": "#0a000000",
  "iconTheme": {"color": "#dd000000"},
  "indicatorColor": "#ffffffff",
  "inputDecorationTheme": {
    "alignLabelWithHint": false,
    "filled": false,
    "floatingLabelAlignment": "start",
    "floatingLabelBehavior": "auto",
    "isCollapsed": false,
    "isDense": false
  },
  "materialTapTargetSize": "shrinkWrap",
  "platform": "macOS",
  "primaryColor": "#ff006688",
  "primaryColorDark": "#ff1976d2",
  "primaryColorLight": "#ffbbdefb",
  "primaryIconTheme": {"color": "#ffffffff"},
  "primaryTextTheme": {
    "bodyLarge": {
      "color": "#ffffffff",
      "decoration": "none",
      "decorationColor": "#ffffffff",
      "fontFamily": ".AppleSystemUIFont",
      "inherit": true
    },
    "bodyMedium": {
      "color": "#ffffffff",
      "decoration": "none",
      "decorationColor": "#ffffffff",
      "fontFamily": ".AppleSystemUIFont",
      "inherit": true
    },
    "bodySmall": {
      "color": "#ffffffff",
      "decoration": "none",
      "decorationColor": "#ffffffff",
      "fontFamily": ".AppleSystemUIFont",
      "inherit": true
    },
    "displayLarge": {
      "color": "#ffffffff",
      "decoration": "none",
      "decorationColor": "#ffffffff",
      "fontFamily": ".AppleSystemUIFont",
      "inherit": true
    },
    "displayMedium": {
      "color": "#ffffffff",
      "decoration": "none",
      "decorationColor": "#ffffffff",
      "fontFamily": ".AppleSystemUIFont",
      "inherit": true
    },
    "displaySmall": {
      "color": "#ffffffff",
      "decoration": "none",
      "decorationColor": "#ffffffff",
      "fontFamily": ".AppleSystemUIFont",
      "inherit": true
    },
    "headlineLarge": {
      "color": "#ffffffff",
      "decoration": "none",
      "decorationColor": "#ffffffff",
      "fontFamily": ".AppleSystemUIFont",
      "inherit": true
    },
    "headlineMedium": {
      "color": "#ffffffff",
      "decoration": "none",
      "decorationColor": "#ffffffff",
      "fontFamily": ".AppleSystemUIFont",
      "inherit": true
    },
    "headlineSmall": {
      "color": "#ffffffff",
      "decoration": "none",
      "decorationColor": "#ffffffff",
      "fontFamily": ".AppleSystemUIFont",
      "inherit": true
    },
    "labelLarge": {
      "color": "#ffffffff",
      "decoration": "none",
      "decorationColor": "#ffffffff",
      "fontFamily": ".AppleSystemUIFont",
      "inherit": true
    },
    "labelMedium": {
      "color": "#ffffffff",
      "decoration": "none",
      "decorationColor": "#ffffffff",
      "fontFamily": ".AppleSystemUIFont",
      "inherit": true
    },
    "labelSmall": {
      "color": "#ffffffff",
      "decoration": "none",
      "decorationColor": "#ffffffff",
      "fontFamily": ".AppleSystemUIFont",
      "inherit": true
    },
    "titleLarge": {
      "color": "#ffffffff",
      "decoration": "none",
      "decorationColor": "#ffffffff",
      "fontFamily": ".AppleSystemUIFont",
      "inherit": true
    },
    "titleMedium": {
      "color": "#ffffffff",
      "decoration": "none",
      "decorationColor": "#ffffffff",
      "fontFamily": ".AppleSystemUIFont",
      "inherit": true
    },
    "titleSmall": {
      "color": "#ffffffff",
      "decoration": "none",
      "decorationColor": "#ffffffff",
      "fontFamily": ".AppleSystemUIFont",
      "inherit": true
    }
  },
  "scaffoldBackgroundColor": "#fffbfcfe",
  "secondaryHeaderColor": "#ffe3f2fd",
  "shadowColor": "#ff000000",
  "splashColor": "#66c8c8c8",
  "splashFactory": "ripple",
  "textTheme": {
    "bodyLarge": {
      "color": "#dd000000",
      "decoration": "none",
      "fontFamily": "Montserrat_regular",
      "fontFamilyFallback": ["Montserrat"],
      "fontSize": 16,
      "fontWeight": "w400",
      "inherit": false,
      "letterSpacing": 0.5,
      "textBaseline": "alphabetic"
    },
    "bodyMedium": {
      "color": "#dd000000",
      "decoration": "none",
      "fontFamily": "Montserrat_regular",
      "fontFamilyFallback": ["Montserrat"],
      "fontSize": 14,
      "fontWeight": "w400",
      "inherit": false,
      "letterSpacing": 0.25,
      "textBaseline": "alphabetic"
    },
    "bodySmall": {
      "color": "#8a000000",
      "decoration": "none",
      "fontFamily": "Montserrat_regular",
      "fontFamilyFallback": ["Montserrat"],
      "fontSize": 12,
      "fontWeight": "w400",
      "inherit": false,
      "letterSpacing": 0.4,
      "textBaseline": "alphabetic"
    },
    "displayLarge": {
      "color": "#8a000000",
      "decoration": "none",
      "fontFamily": "Montserrat_300",
      "fontFamilyFallback": ["Montserrat"],
      "fontSize": 96,
      "fontWeight": "w300",
      "inherit": false,
      "letterSpacing": -1.5,
      "textBaseline": "alphabetic"
    },
    "displayMedium": {
      "color": "#8a000000",
      "decoration": "none",
      "fontFamily": "Montserrat_300",
      "fontFamilyFallback": ["Montserrat"],
      "fontSize": 60,
      "fontWeight": "w300",
      "inherit": false,
      "letterSpacing": -0.5,
      "textBaseline": "alphabetic"
    },
    "displaySmall": {
      "color": "#8a000000",
      "decoration": "none",
      "fontFamily": "Montserrat_regular",
      "fontFamilyFallback": ["Montserrat"],
      "fontSize": 48,
      "fontWeight": "w400",
      "inherit": false,
      "letterSpacing": 0,
      "textBaseline": "alphabetic"
    },
    "headlineLarge": {
      "color": "#8a000000",
      "decoration": "none",
      "fontFamily": "Montserrat_regular",
      "fontFamilyFallback": ["Montserrat"],
      "fontSize": 40,
      "fontWeight": "w400",
      "inherit": false,
      "letterSpacing": 0.25,
      "textBaseline": "alphabetic"
    },
    "headlineMedium": {
      "color": "#8a000000",
      "decoration": "none",
      "fontFamily": "Montserrat_regular",
      "fontFamilyFallback": ["Montserrat"],
      "fontSize": 34,
      "fontWeight": "w400",
      "inherit": false,
      "letterSpacing": 0.25,
      "textBaseline": "alphabetic"
    },
    "headlineSmall": {
      "color": "#dd000000",
      "decoration": "none",
      "fontFamily": "Montserrat_regular",
      "fontFamilyFallback": ["Montserrat"],
      "fontSize": 24,
      "fontWeight": "w400",
      "inherit": false,
      "letterSpacing": 0,
      "textBaseline": "alphabetic"
    },
    "labelLarge": {
      "color": "#dd000000",
      "decoration": "none",
      "fontFamily": "Montserrat_500",
      "fontFamilyFallback": ["Montserrat"],
      "fontSize": 14,
      "fontWeight": "w500",
      "inherit": false,
      "letterSpacing": 1.25,
      "textBaseline": "alphabetic"
    },
    "labelMedium": {
      "color": "#ff000000",
      "decoration": "none",
      "fontFamily": "Montserrat_regular",
      "fontFamilyFallback": ["Montserrat"],
      "fontSize": 11,
      "fontWeight": "w400",
      "inherit": false,
      "letterSpacing": 1.5,
      "textBaseline": "alphabetic"
    },
    "labelSmall": {
      "color": "#ff000000",
      "decoration": "none",
      "fontFamily": "Montserrat_regular",
      "fontFamilyFallback": ["Montserrat"],
      "fontSize": 10,
      "fontWeight": "w400",
      "inherit": false,
      "letterSpacing": 1.5,
      "textBaseline": "alphabetic"
    },
    "titleLarge": {
      "color": "#dd000000",
      "decoration": "none",
      "fontFamily": "Montserrat_500",
      "fontFamilyFallback": ["Montserrat"],
      "fontSize": 20,
      "fontWeight": "w500",
      "inherit": false,
      "letterSpacing": 0.15,
      "textBaseline": "alphabetic"
    },
    "titleMedium": {
      "color": "#dd000000",
      "decoration": "none",
      "fontFamily": "Montserrat_regular",
      "fontFamilyFallback": ["Montserrat"],
      "fontSize": 16,
      "fontWeight": "w400",
      "inherit": false,
      "letterSpacing": 0.15,
      "textBaseline": "alphabetic"
    },
    "titleSmall": {
      "color": "#ff000000",
      "decoration": "none",
      "fontFamily": "Montserrat_500",
      "fontFamilyFallback": ["Montserrat"],
      "fontSize": 14,
      "fontWeight": "w500",
      "inherit": false,
      "letterSpacing": 0.1,
      "textBaseline": "alphabetic"
    }
  },
  "typography": {
    "black": {
      "bodyLarge": {
        "color": "#ff191c1e",
        "decoration": "none",
        "decorationColor": "#ff191c1e",
        "fontFamily": ".AppleSystemUIFont",
        "inherit": true
      },
      "bodyMedium": {
        "color": "#ff191c1e",
        "decoration": "none",
        "decorationColor": "#ff191c1e",
        "fontFamily": ".AppleSystemUIFont",
        "inherit": true
      },
      "bodySmall": {
        "color": "#ff191c1e",
        "decoration": "none",
        "decorationColor": "#ff191c1e",
        "fontFamily": ".AppleSystemUIFont",
        "inherit": true
      },
      "displayLarge": {
        "color": "#ff191c1e",
        "decoration": "none",
        "decorationColor": "#ff191c1e",
        "fontFamily": ".AppleSystemUIFont",
        "inherit": true
      },
      "displayMedium": {
        "color": "#ff191c1e",
        "decoration": "none",
        "decorationColor": "#ff191c1e",
        "fontFamily": ".AppleSystemUIFont",
        "inherit": true
      },
      "displaySmall": {
        "color": "#ff191c1e",
        "decoration": "none",
        "decorationColor": "#ff191c1e",
        "fontFamily": ".AppleSystemUIFont",
        "inherit": true
      },
      "headlineLarge": {
        "color": "#ff191c1e",
        "decoration": "none",
        "decorationColor": "#ff191c1e",
        "fontFamily": ".AppleSystemUIFont",
        "inherit": true
      },
      "headlineMedium": {
        "color": "#ff191c1e",
        "decoration": "none",
        "decorationColor": "#ff191c1e",
        "fontFamily": ".AppleSystemUIFont",
        "inherit": true
      },
      "headlineSmall": {
        "color": "#ff191c1e",
        "decoration": "none",
        "decorationColor": "#ff191c1e",
        "fontFamily": ".AppleSystemUIFont",
        "inherit": true
      },
      "labelLarge": {
        "color": "#ff191c1e",
        "decoration": "none",
        "decorationColor": "#ff191c1e",
        "fontFamily": ".AppleSystemUIFont",
        "inherit": true
      },
      "labelMedium": {
        "color": "#ff191c1e",
        "decoration": "none",
        "decorationColor": "#ff191c1e",
        "fontFamily": ".AppleSystemUIFont",
        "inherit": true
      },
      "labelSmall": {
        "color": "#ff191c1e",
        "decoration": "none",
        "decorationColor": "#ff191c1e",
        "fontFamily": ".AppleSystemUIFont",
        "inherit": true
      },
      "titleLarge": {
        "color": "#ff191c1e",
        "decoration": "none",
        "decorationColor": "#ff191c1e",
        "fontFamily": ".AppleSystemUIFont",
        "inherit": true
      },
      "titleMedium": {
        "color": "#ff191c1e",
        "decoration": "none",
        "decorationColor": "#ff191c1e",
        "fontFamily": ".AppleSystemUIFont",
        "inherit": true
      },
      "titleSmall": {
        "color": "#ff191c1e",
        "decoration": "none",
        "decorationColor": "#ff191c1e",
        "fontFamily": ".AppleSystemUIFont",
        "inherit": true
      }
    },
    "dense": {
      "bodyLarge": {
        "fontSize": 16,
        "fontWeight": "w400",
        "height": 1.5,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0.5,
        "textBaseline": "ideographic"
      },
      "bodyMedium": {
        "fontSize": 14,
        "fontWeight": "w400",
        "height": 1.43,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0.25,
        "textBaseline": "ideographic"
      },
      "bodySmall": {
        "fontSize": 12,
        "fontWeight": "w400",
        "height": 1.33,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0.4,
        "textBaseline": "ideographic"
      },
      "displayLarge": {
        "fontSize": 57,
        "fontWeight": "w400",
        "height": 1.12,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": -0.25,
        "textBaseline": "ideographic"
      },
      "displayMedium": {
        "fontSize": 45,
        "fontWeight": "w400",
        "height": 1.16,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0,
        "textBaseline": "ideographic"
      },
      "displaySmall": {
        "fontSize": 36,
        "fontWeight": "w400",
        "height": 1.22,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0,
        "textBaseline": "ideographic"
      },
      "headlineLarge": {
        "fontSize": 32,
        "fontWeight": "w400",
        "height": 1.25,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0,
        "textBaseline": "ideographic"
      },
      "headlineMedium": {
        "fontSize": 28,
        "fontWeight": "w400",
        "height": 1.29,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0,
        "textBaseline": "ideographic"
      },
      "headlineSmall": {
        "fontSize": 24,
        "fontWeight": "w400",
        "height": 1.33,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0,
        "textBaseline": "ideographic"
      },
      "labelLarge": {
        "fontSize": 14,
        "fontWeight": "w500",
        "height": 1.43,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0.1,
        "textBaseline": "ideographic"
      },
      "labelMedium": {
        "fontSize": 12,
        "fontWeight": "w500",
        "height": 1.33,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0.5,
        "textBaseline": "ideographic"
      },
      "labelSmall": {
        "fontSize": 11,
        "fontWeight": "w500",
        "height": 1.45,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0.5,
        "textBaseline": "ideographic"
      },
      "titleLarge": {
        "fontSize": 22,
        "fontWeight": "w400",
        "height": 1.27,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0,
        "textBaseline": "ideographic"
      },
      "titleMedium": {
        "fontSize": 16,
        "fontWeight": "w500",
        "height": 1.5,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0.15,
        "textBaseline": "ideographic"
      },
      "titleSmall": {
        "fontSize": 14,
        "fontWeight": "w500",
        "height": 1.43,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0.1,
        "textBaseline": "ideographic"
      }
    },
    "englishLike": {
      "bodyLarge": {
        "fontSize": 16,
        "fontWeight": "w400",
        "height": 1.5,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0.5,
        "textBaseline": "alphabetic"
      },
      "bodyMedium": {
        "fontSize": 14,
        "fontWeight": "w400",
        "height": 1.43,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0.25,
        "textBaseline": "alphabetic"
      },
      "bodySmall": {
        "fontSize": 12,
        "fontWeight": "w400",
        "height": 1.33,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0.4,
        "textBaseline": "alphabetic"
      },
      "displayLarge": {
        "fontSize": 57,
        "fontWeight": "w400",
        "height": 1.12,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": -0.25,
        "textBaseline": "alphabetic"
      },
      "displayMedium": {
        "fontSize": 45,
        "fontWeight": "w400",
        "height": 1.16,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0,
        "textBaseline": "alphabetic"
      },
      "displaySmall": {
        "fontSize": 36,
        "fontWeight": "w400",
        "height": 1.22,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0,
        "textBaseline": "alphabetic"
      },
      "headlineLarge": {
        "fontSize": 32,
        "fontWeight": "w400",
        "height": 1.25,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0,
        "textBaseline": "alphabetic"
      },
      "headlineMedium": {
        "fontSize": 28,
        "fontWeight": "w400",
        "height": 1.29,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0,
        "textBaseline": "alphabetic"
      },
      "headlineSmall": {
        "fontSize": 24,
        "fontWeight": "w400",
        "height": 1.33,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0,
        "textBaseline": "alphabetic"
      },
      "labelLarge": {
        "fontSize": 14,
        "fontWeight": "w500",
        "height": 1.43,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0.1,
        "textBaseline": "alphabetic"
      },
      "labelMedium": {
        "fontSize": 12,
        "fontWeight": "w500",
        "height": 1.33,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0.5,
        "textBaseline": "alphabetic"
      },
      "labelSmall": {
        "fontSize": 11,
        "fontWeight": "w500",
        "height": 1.45,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0.5,
        "textBaseline": "alphabetic"
      },
      "titleLarge": {
        "fontSize": 22,
        "fontWeight": "w400",
        "height": 1.27,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0,
        "textBaseline": "alphabetic"
      },
      "titleMedium": {
        "fontSize": 16,
        "fontWeight": "w500",
        "height": 1.5,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0.15,
        "textBaseline": "alphabetic"
      },
      "titleSmall": {
        "fontSize": 14,
        "fontWeight": "w500",
        "height": 1.43,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0.1,
        "textBaseline": "alphabetic"
      }
    },
    "tall": {
      "bodyLarge": {
        "fontSize": 16,
        "fontWeight": "w400",
        "height": 1.5,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0.5,
        "textBaseline": "alphabetic"
      },
      "bodyMedium": {
        "fontSize": 14,
        "fontWeight": "w400",
        "height": 1.43,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0.25,
        "textBaseline": "alphabetic"
      },
      "bodySmall": {
        "fontSize": 12,
        "fontWeight": "w400",
        "height": 1.33,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0.4,
        "textBaseline": "alphabetic"
      },
      "displayLarge": {
        "fontSize": 57,
        "fontWeight": "w400",
        "height": 1.12,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": -0.25,
        "textBaseline": "alphabetic"
      },
      "displayMedium": {
        "fontSize": 45,
        "fontWeight": "w400",
        "height": 1.16,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0,
        "textBaseline": "alphabetic"
      },
      "displaySmall": {
        "fontSize": 36,
        "fontWeight": "w400",
        "height": 1.22,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0,
        "textBaseline": "alphabetic"
      },
      "headlineLarge": {
        "fontSize": 32,
        "fontWeight": "w400",
        "height": 1.25,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0,
        "textBaseline": "alphabetic"
      },
      "headlineMedium": {
        "fontSize": 28,
        "fontWeight": "w400",
        "height": 1.29,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0,
        "textBaseline": "alphabetic"
      },
      "headlineSmall": {
        "fontSize": 24,
        "fontWeight": "w400",
        "height": 1.33,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0,
        "textBaseline": "alphabetic"
      },
      "labelLarge": {
        "fontSize": 14,
        "fontWeight": "w500",
        "height": 1.43,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0.1,
        "textBaseline": "alphabetic"
      },
      "labelMedium": {
        "fontSize": 12,
        "fontWeight": "w500",
        "height": 1.33,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0.5,
        "textBaseline": "alphabetic"
      },
      "labelSmall": {
        "fontSize": 11,
        "fontWeight": "w500",
        "height": 1.45,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0.5,
        "textBaseline": "alphabetic"
      },
      "titleLarge": {
        "fontSize": 22,
        "fontWeight": "w400",
        "height": 1.27,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0,
        "textBaseline": "alphabetic"
      },
      "titleMedium": {
        "fontSize": 16,
        "fontWeight": "w500",
        "height": 1.5,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0.15,
        "textBaseline": "alphabetic"
      },
      "titleSmall": {
        "fontSize": 14,
        "fontWeight": "w500",
        "height": 1.43,
        "inherit": false,
        "leadingDistribution": "even",
        "letterSpacing": 0.1,
        "textBaseline": "alphabetic"
      }
    },
    "white": {
      "bodyLarge": {
        "color": "#fffbfcfe",
        "decoration": "none",
        "decorationColor": "#fffbfcfe",
        "fontFamily": ".AppleSystemUIFont",
        "inherit": true
      },
      "bodyMedium": {
        "color": "#fffbfcfe",
        "decoration": "none",
        "decorationColor": "#fffbfcfe",
        "fontFamily": ".AppleSystemUIFont",
        "inherit": true
      },
      "bodySmall": {
        "color": "#fffbfcfe",
        "decoration": "none",
        "decorationColor": "#fffbfcfe",
        "fontFamily": ".AppleSystemUIFont",
        "inherit": true
      },
      "displayLarge": {
        "color": "#fffbfcfe",
        "decoration": "none",
        "decorationColor": "#fffbfcfe",
        "fontFamily": ".AppleSystemUIFont",
        "inherit": true
      },
      "displayMedium": {
        "color": "#fffbfcfe",
        "decoration": "none",
        "decorationColor": "#fffbfcfe",
        "fontFamily": ".AppleSystemUIFont",
        "inherit": true
      },
      "displaySmall": {
        "color": "#fffbfcfe",
        "decoration": "none",
        "decorationColor": "#fffbfcfe",
        "fontFamily": ".AppleSystemUIFont",
        "inherit": true
      },
      "headlineLarge": {
        "color": "#fffbfcfe",
        "decoration": "none",
        "decorationColor": "#fffbfcfe",
        "fontFamily": ".AppleSystemUIFont",
        "inherit": true
      },
      "headlineMedium": {
        "color": "#fffbfcfe",
        "decoration": "none",
        "decorationColor": "#fffbfcfe",
        "fontFamily": ".AppleSystemUIFont",
        "inherit": true
      },
      "headlineSmall": {
        "color": "#fffbfcfe",
        "decoration": "none",
        "decorationColor": "#fffbfcfe",
        "fontFamily": ".AppleSystemUIFont",
        "inherit": true
      },
      "labelLarge": {
        "color": "#fffbfcfe",
        "decoration": "none",
        "decorationColor": "#fffbfcfe",
        "fontFamily": ".AppleSystemUIFont",
        "inherit": true
      },
      "labelMedium": {
        "color": "#fffbfcfe",
        "decoration": "none",
        "decorationColor": "#fffbfcfe",
        "fontFamily": ".AppleSystemUIFont",
        "inherit": true
      },
      "labelSmall": {
        "color": "#fffbfcfe",
        "decoration": "none",
        "decorationColor": "#fffbfcfe",
        "fontFamily": ".AppleSystemUIFont",
        "inherit": true
      },
      "titleLarge": {
        "color": "#fffbfcfe",
        "decoration": "none",
        "decorationColor": "#fffbfcfe",
        "fontFamily": ".AppleSystemUIFont",
        "inherit": true
      },
      "titleMedium": {
        "color": "#fffbfcfe",
        "decoration": "none",
        "decorationColor": "#fffbfcfe",
        "fontFamily": ".AppleSystemUIFont",
        "inherit": true
      },
      "titleSmall": {
        "color": "#fffbfcfe",
        "decoration": "none",
        "decorationColor": "#fffbfcfe",
        "fontFamily": ".AppleSystemUIFont",
        "inherit": true
      }
    }
  },
  "unselectedWidgetColor": "#8a000000",
  "useMaterial3": true,
  "visualDensity": "compact"
};
