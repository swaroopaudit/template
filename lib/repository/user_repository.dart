import 'package:dio/dio.dart';
import 'package:template/models/user.dart';
import 'package:template/services/api_service.dart';

class UserRepository {
  static Future<List<User>?> fetchUsers() async {
    try {
      Response? response = await API.get(endpoint: '/users');
      if (response == null) {
        return null;
      } else {
        var data = response.data['data'];
        List<dynamic> users = data.map((json) => User.fromJson(json)).toList();
        List<User> typedUsers =
            users.map((dynamic user) => user as User).toList();
        return typedUsers;
      }
    } catch (error) {
      return null;
    }
  }
}
