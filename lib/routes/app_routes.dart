import 'package:template/presentation/feature/home/view/home_screen.dart';
import 'package:template/routes/navigation_keys.dart';
import 'package:flutter/material.dart';
import 'package:get/get.dart';

Widget kHomeScreen() => Navigator(
      key: Get.nestedKey(NavigationKeys.home.index),
      onGenerateRoute: (routeSettings) => GetPageRoute(
        page: () => HomeScreen(),
      ),
    );
