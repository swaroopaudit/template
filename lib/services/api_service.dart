import 'package:amplify_flutter/amplify_flutter.dart';
import 'package:dio/dio.dart';
import 'package:flutter_dotenv/flutter_dotenv.dart';

class API {
  static final dio = Dio();

  static Future<Response?> get({required String endpoint}) async {
    try {
      final String? route = dotenv.env['ENDPOINT'];

      if (route == null) {
        return null;
      }

      final response = await dio.get('$route$endpoint');
      if (response.statusCode == 200) {
        return response;
      } else {
        return null;
      }
    } catch (error) {
      return null;
    }
  }

  static Future<Response?> put({
    required String endpoint,
    required Map<String, dynamic> data,
  }) async {
    try {
      final response = await Amplify.API
          .post(
            endpoint,
            body: HttpPayload.json(data),
          )
          .response;

      if (response.statusCode == 200) {
        return Response(
          requestOptions: RequestOptions(),
          statusCode: 200,
        );
      } else {
        return null;
      }
    } catch (error) {
      return null;
    }
  }
}
