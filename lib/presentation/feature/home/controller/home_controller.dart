import 'package:get/get.dart';
import 'package:template/models/user.dart';
import 'package:template/repository/user_repository.dart';

class HomeController extends GetxController {
  RxBool isLoading = true.obs;
  RxList<User> users = <User>[].obs;

  getUsers() async {
    try {
      isLoading.value = true;
      var fetchedUsers = await UserRepository.fetchUsers();
      if (fetchedUsers == null) {
        throw Error();
      } else {
        this.users.value = fetchedUsers;
        await Future.delayed(Duration(seconds: 2));
        isLoading.value = false;
      }
    } catch (error) {
      isLoading.value = false;
      //TODO:  Handle Error
    }
  }
}
