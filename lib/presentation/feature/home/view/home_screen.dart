import 'package:cached_network_image/cached_network_image.dart';
import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:get/get.dart';
import 'package:template/presentation/feature/home/controller/home_controller.dart';
import 'package:template/presentation/ui_components/common/common_widgets.dart';

class HomeScreen extends GetView<HomeController> {
  HomeScreen({super.key}) {
    controller.getUsers();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Obx(
        () => controller.isLoading.value
            ? CommonWidgets.getCircularIndicator()
            : getUsers(),
      ),
      floatingActionButton: FloatingActionButton(
        child: Icon(Icons.refresh),
        onPressed: () => controller.getUsers(),
      ),
    );
  }

  Widget getUsers() {
    return ListView.separated(
      itemCount: controller.users.length,
      separatorBuilder: (context, index) => Divider(
        endIndent: 20.w,
        indent: 20.w,
        color: Colors.black.withOpacity(0.1),
      ),
      itemBuilder: (context, index) => ListTile(
        leading: CircleAvatar(
          radius: 30.r,
          backgroundImage: CachedNetworkImageProvider(
            controller.users[index].avatar ?? '',
            scale: 1.0,
          ),
        ),
        title: Text(
            '${controller.users[index].firstName} ${controller.users[index].lastName}'),
        subtitle: Text(controller.users[index].email ?? ''),
      ),
    );
  }
}
