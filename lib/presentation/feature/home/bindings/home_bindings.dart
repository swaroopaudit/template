import 'package:template/presentation/feature/home/controller/home_controller.dart';
import 'package:get/get.dart';

class HomeBindings implements Bindings {
  HomeBindings();

  @override
  void dependencies() {
    Get.lazyPut(() => HomeController());
  }
}
