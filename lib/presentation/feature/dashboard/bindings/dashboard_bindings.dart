import 'package:template/presentation/feature/home/controller/home_controller.dart';
import 'package:template/presentation/feature/dashboard/controller/navigation_controller.dart';
import 'package:get/get.dart';

class DashboardBindings implements Bindings {
  DashboardBindings();

  @override
  void dependencies() {
    Get.lazyPut(() => NavigationController());
    Get.lazyPut(() => HomeController());
  }
}
