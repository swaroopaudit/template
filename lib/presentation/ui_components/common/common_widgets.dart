import 'package:flutter/material.dart';

class CommonWidgets {
  static Widget getCircularIndicator() {
    return Center(
      child: CircularProgressIndicator(),
    );
  }
}
